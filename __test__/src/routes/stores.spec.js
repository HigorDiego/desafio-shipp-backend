const app = require('../../../index')
const { IntegrationRoutes } = require('../../helpers/integrationRoutes')


const object = {
    lat: '42.73063',
    long: '-73.703443'
}

const test_stores = {
    describe: `Routes Stores`,
    object: {
        get: [
            {
                it: {
                    describe: `Route get ${'/api/stores'}`,
                    description: `should send not params lat e long error 400`,
                    url: '/api/stores/',
                    token: '',
                    be: 'array',
                    status: 400,
                    body: {}
                }
            },
            {
                it: {
                    describe: `Route get /api/stores?lat=${object.lat}`,
                    description: `should send not params long error 400`,
                    url: `/api/stores?lat=${object.lat}`,
                    token: '',
                    be: 'array',
                    status: 400,
                    body: {}
                }
            },
            {
                it: {
                    describe: `Route get /api/stores?long=${object.long}`,
                    description: `should send not params lat error 400`,
                    url: `/api/stores?long=${object.long}`,
                    token: '',
                    be: 'array',
                    status: 400,
                    body: {}
                }
            },
            {
                it: {
                    describe: `Route get /api/stores?lat=""&long=""`,
                    description: `should send params lat and long empty, error 400`,
                    url: `/api/stores?lat=""&long=""`,
                    token: '',
                    be: 'array',
                    status: 400,
                    body: {}
                }
            },
            {
                it: {
                    describe: `Route get /api/stores?lat=""`,
                    description: `should send params latempty, error 400`,
                    url: `/api/stores?lat=""`,
                    token: '',
                    be: 'array',
                    status: 400,
                    body: {}
                }
            },
            {
                it: {
                    describe: `Route get /api/stores?long=""`,
                    description: `should send params long empty, error 400`,
                    url: `/api/stores?&long=""`,
                    token: '',
                    be: 'array',
                    status: 400,
                    body: {}
                }
            },
            {
                it: {
                    describe: `Route get /api/stores?lat="a"`,
                    description: `should send params lat not number, error 400`,
                    url: `/api/stores?lat="a"`,
                    token: '',
                    be: 'array',
                    status: 400,
                    body: {}
                }
            },
            {
                it: {
                    describe: `Route get /api/stores?long="a"`,
                    description: `should send params long not number, error 400`,
                    url: `/api/stores?long="a"`,
                    token: '',
                    be: 'array',
                    status: 400,
                    body: {}
                }
            },
            {
                it: {
                    describe: `Route get /api/stores?lat=${object.lat}`,
                    description: `should send params lat number, error 400`,
                    url: `/api/stores?long="42.73063"`,
                    token: '',
                    be: 'array',
                    status: 400,
                    body: {}
                }
            },

            {
                it: {
                    describe: `Route get /api/stores?long=${object.long}`,
                    description: `should send params long number, error 400`,
                    url: `/api/stores?long=${object.long}`,
                    token: '',
                    be: 'array',
                    status: 400,
                    body: {}
                }
            },

            {
                it: {
                    describe: `Route get /api/stores?lat=${object.lat}&long=${object.long}`,
                    description: `should send params lat and long number, return 200`,
                    url: `/api/stores?lat=${object.lat}&long=${object.long}`,
                    token: '',
                    be: 'array',
                    status: 200,
                    body: {}
                }
            }
        ]
    }
}
IntegrationRoutes(app)(test_stores)