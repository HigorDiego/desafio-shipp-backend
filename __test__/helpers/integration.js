var request = require('supertest')
module.exports = ({
    defaultGet: (app, url, status, token, be, done) => {
        request(app)
            .get(url)
            .set('token', token)
            .end((_, res) => {
                res.status.should.be.eql(status)
                if (res.body !== null) res.body.should.be.an(be)
                done()
            })
    }
})
