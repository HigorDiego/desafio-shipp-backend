const integration = require('./integration')

const describeGet = (value) => (app) => {
    describe(value.describe, () => {
        it(value.description, function (done) {
            integration.defaultGet(app, value.url, value.status, value.token, value.be, done)
        })
    })
}
module.exports.IntegrationRoutes = (app) => (json) => {


    describe(json.describe, () => {

        // get
        if (json.object.get.length > 0) json.object.get.map(value => describeGet(value.it)(app))

    })
}