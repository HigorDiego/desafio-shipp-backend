(async () => {
    try {
        const {
            parseJsonLocation,
            readCsvFileConvertJson,
            keysMaps,
            renameKeys,
            renameObjectLocation,
            normalizeArrayStruct,
            structObject,
            saveStructDatabase
        } = require('./helper')

        console.log('--------- init import ---------')

        const csvFilePath = 'stores.csv'

        const readFile = await readCsvFileConvertJson(csvFilePath)
        const parseObject = renameObjectLocation(readFile, parseJsonLocation)

        const listOldKey = Object.keys(parseObject[0])
        const newKey = [
            'county', 'license', 'operation_type', 'establishment_type', 'entity_name', 'dba_name',
            'street_number', 'street_name', 'address_Line_2', 'address_line_3', 'city', 'state', 'zip_code',
            'square_footage', 'location'
        ]
        const keys = keysMaps(newKey, listOldKey)

        const newObject = normalizeArrayStruct(parseObject, keys, renameKeys)

        const addressStruct = ['street_name', 'street_number', 'address_line_2', 'address_line_3', 'zip_code', 'city', 'state', 'latitude', 'longitude', 'square_footage', 'needs_recoding']

        const storeStruct = ['license', 'entity_name', 'operation_type', 'establishment_type', 'dba_name']

        const locationStruct = ['longitude', 'needs_recoding', 'latitude']

        const humanStruct = ['address', 'city', 'state', 'zip']

        await saveStructDatabase(addressStruct, storeStruct, locationStruct, humanStruct)(newObject, structObject)

        console.log('--------- finish import ---------')

    } catch (err) {
        console.warn(err)
    }
})()
