const csv = require('csvtojson')
const { store, address } = require('../src/database/models')

/**
     * @function parseJsonLocation - Função responsável por passar a string de Location em JSON.
     * @param  {String} location - String contem um json como um template string
     * @return {{Location: {longitude: String,needs_recoding: Boolean,  human_address: {address: String, city: String, state: String, zip: String} latitude: String}}} - Location objeto de retorno
     * @example 
     * Retorno
         Location: {
             longitude: '-73.700539',
            needs_recoding: false,
            human_address: {
            address: '601 SARATOGA ST',
            city: 'COHOES',
            state: 'NY',
            zip: '12047'
            },
            latitude: '42.754424'
        }
*/
exports.parseJsonLocation = (location) => JSON.parse(location.replace(/'/g, '"').replace(/"{/g, '{').replace(/}"/g, '}').replace('False', false).replace("True", true))

/**
 * @function readCsvFileConvertJson
 * @package csvtojson - https://github.com/Keyang/node-csvtojson
 * @param  {String} csvFilePath - Nome do arquivo e o path para leitura.
 * @return {Promise}
 */
exports.readCsvFileConvertJson = (csvFilePath) => csv(csvFilePath).fromFile(csvFilePath)

/**
 * @function renameKeys
 * @description Função reponsável por passar o antigaskeys correspondente  para o novo formato
 * @link https://developer.mozilla.org/pt-BR/docs/Web/JavaScript/Reference/Global_Objects/Array/reduce
 * @link https://developer.mozilla.org/pt-BR/docs/Web/JavaScript/Reference/Global_Objects/Object/keys
 * @param  {Object} keysMap -  KeyMaps como objeto para passar as novas keys.
 * @param  {Object} obj -  objecto com as keys antigas.
 * @Retunn 
 */
exports.renameKeys = (keysMap) => (obj) =>
    Object.keys(obj).reduce(
        (acc, key) => ({
            ...acc,
            ...{ [keysMap[key] || key]: obj[key] }
        }),
        {}
    );


/**
 * @function KeysMaps
 * @description função responsável por gerar as keys antigas com as correspondente.
 * @param  {[
    'county', 'license', 'operation_type', 'establishment_type', 'entity_name', 'dba_name',
    'street_number', 'street_name', 'address_Line_2', 'address_line_3',
    'city', 'state', 'zip_code', 'square_footage', 'location'
    ]} newKey - As novas keys.
 * @param  {[ 'County', 'License Number', 'Operation Type', 'Establishment Type', 
            'Entity Name', 'DBA Name', 'Street Number',  'Street Name', 'Address Line 2', 
        'Address Line 3', 'City', 'State', 'Zip Code','Square Footage', 'Location'
    ]} listOldKey - as antigas keys que serão modificadas
 * @example
 * Retorno
 * {
        County: 'county',
        'License Number': 'license',
        'Operation Type': 'operation_type',
        'Establishment Type': 'establishment_type',
        'Entity Name': 'entity_name',
        'DBA Name': 'dba_name',
        'Street Number': 'street_number',
        'Street Name': 'street_name',
        'Address Line 2': 'address_Line_2',
        'Address Line 3': 'address_line_3',
        City: 'city',
        State: 'state',
        'Zip Code': 'zip_code',
        'Square Footage': 'square_footage',
        Location: 'location'
        }
 *
 */
exports.keysMaps = (newKey, listOldKey) =>
    listOldKey.reduce((acc, key, i) => {
        acc[key] = newKey[i]
        return acc
    }, {})


/**
 * @function normalizeArrayStruct
 * @description Função responsável por normalizar as keys do array de objeto.
 * @param  {Array} object
 * @param  {Object} keysMap
 * @param  {Function} renameKeys - função reponsável por modificar as keys do objeto. 
 * @return {Array}
 */
exports.normalizeArrayStruct = (object, keysMap, renameKeys) => object.map(renameKeys(keysMap))

/**
 * @function renameObjectLocation
 * @description função responsável por passar o campo location de string para JSON.
 * @param  {Array} object - Objeto que será processado para um nova key Location
 * @param  {Function} parseJsonLocation - Função responsável por passar a string de location em objeto JSON.
 * @return {Object}
 */
exports.renameObjectLocation = (object, parseJsonLocation) => object.map((value) => ({
    ...value,
    ...{ Location: value.Location ? parseJsonLocation(value.Location) : {} }
}))

/**
 * @function structObject
 * @description
 * @param  {Object} struct
 * @param  {Object} object
 * @return {Object}
 */
exports.structObject = (struct, object) => object ? JSON.parse(JSON.stringify(object, struct)) : {}

/**
 * @function saveStructDatabase
 * @description função que valida os dados que estão no banco caso não tenha aquele objeto insere o mesmo.
 * @param  {Array} structAddress - estrutura de dados de endereço.
 * @param  {Array} structStore - estrutura de dados de loja.
 * @param  {Array} locationStruct - estrutura de dados da localização
 * @param  {Array} humanStruct - estrutura de humans.
 * @param  {Array} object - Objeto em array com os atributos necessários para inserção.
 * @param  {Function} fn - função que faz o parse do objeto.
 * @return {Promise}
 */
exports.saveStructDatabase = (structAddress, structStore, locationStruct, humanStruct) => async (object, fn) => {
    try {
        return Promise.all(object.map(async value => {
            const validate = await store.findOne({ where: fn(structStore, value), raw: true })
            if (validate === null) {
                const result = await store.create(fn(structStore, value))
                await address.create({
                    ...fn(structAddress, value),
                    ...{ store_id: result.dataValues.id },
                    ...fn(locationStruct, value.location),
                    ...fn(humanStruct, value.location.human_address)
                })
            }
        }))
    } catch (err) {
        throw new Error(err)
    }
}