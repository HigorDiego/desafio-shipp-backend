# Processo Seletivo 

    Candidate: Higor Diego

# Install dependencies

- Dependencies Application: `yarn install`
- Dependencies Documentation: `yarn add apidoc live-server --global`
- Dependencies Sequelize: `yarn add sequelize-cli -g`

# Import csv

### Create database
`yarn run migrate`

### Import csv
`yarn run import`

# Generate Documentation
` apidoc -e "(node_modules|public|__test_)" -o doc-html /apidoc `

# Machine Local
### Technologies needed
- Nodejs v12.13.1 or superior
- Docker v2.1.0.5 or superior

### Start Documentation
`yarn run doc`

### Tester
` yarn run test`

### Eslint
` yarn run lint:fix `

### Start Application
`yarn start`

### Read Coverage Test
`yarn run coverage`

### Logs Api
`cd $PWD/log && cat *.log`

### Access
- Api: *http://localhost:3000*
- Documentation: *http://localhost:8080*

# Docker
### Tecnologies needed
- Docker 19.03.5
- Docker Compose 1.24.1

## Start Application
`docker-compose up --build`

### Access
- Api: *http://localhost:3000*
- Documentation: *http://localhost:8080*



