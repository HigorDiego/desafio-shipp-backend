/**
    * @api {get} /api/v1/stores  Get Stores
    * @apiGroup Stores
    * @apiVersion 1.0.0
    * @apiSuccess {Float} lat Stores lat - Params cannot be empty and not numeric
    * @apiSuccess {Float} long Stores long - Params cannot be empty and not numeric
    * @apiExample {curl} Example:
        curl http://localhost:3000/api/stores?lat=42.73063&long=-73.703443 \
        -H 'Cache-Control: no-cache'
    * @apiSuccessExample {json} Success
    *    HTTP/1.1 200 OK
        [
            {
            "id": 1,
            "license": "720557",
            "entity_name": "SAEED SADIQ, SAIKA NOREEN",
            "operation_type": "Store",
            "establishment_type": "JAC",
            "dba_name": "19 STREET QUICK STOP",
            "createdAt": "2019-12-23T22:57:25.130Z",
            "updatedAt": "2019-12-23T22:57:25.130Z",
            "addresses": [
            {
                "id": 1,
                "street_name": "19TH STREET",
                "street_number": "315",
                "address_line_2": null,
                "address_line_3": "",
                "zip_code": "12189",
                "city": "WATERVLIET",
                "state": "NY",
                "latitude": 42.73063,
                "longitude": -73.703443,
                "square_footage": 1200,
                "needs_recoding": false,
                "createdAt": "2019-12-23T22:57:51.673Z",
                "updatedAt": "2019-12-23T22:57:51.673Z",
                "store_id": 1
            }
                "distance": "0.00"
            }
        ]
    * @apiErrorExample {json} Empty Params
    *    HTTP/1.1 400 Bad Request
    [
        {
            "title": "Error",
            "message": "Latitude required!"
        },
        {
            "title": "Error",
            "message": "Long required!"
        }
    ]
    * @apiErrorExample {json} Stores error
    *    HTTP/1.1 500 Internal Server Error
**/
