module.exports = (sequelize, DataTypes) => {
    const Store = sequelize.define('store', {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },

        license: {
            type: DataTypes.STRING
        },
        entity_name: {
            type: DataTypes.STRING
        },
        operation_type: {
            type: DataTypes.STRING
        },
        establishment_type: {
            type: DataTypes.STRING
        },
        dba_name: {
            type: DataTypes.STRING
        },

    })

    Store.associate = models => {
        Store.hasMany(models.address, { foreignKey: 'store_id' })
    }
    return Store
}
