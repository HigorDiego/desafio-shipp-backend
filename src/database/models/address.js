module.exports = (sequelize, DataTypes) => {
    const Address = sequelize.define('address', {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },

        street_name: {
            type: DataTypes.STRING
        },
        street_number: {
            type: DataTypes.STRING,
            allowNull: true
        },
        address_line_2: {
            type: DataTypes.STRING,
        },
        address_line_3: {
            type: DataTypes.STRING
        },
        zip_code: {
            type: DataTypes.STRING
        },
        city: {
            type: DataTypes.STRING
        },
        state: {
            type: DataTypes.STRING
        },
        latitude: {
            type: DataTypes.DOUBLE,
        },
        longitude: {
            type: DataTypes.DOUBLE,
        },
        square_footage: {
            type: DataTypes.DOUBLE,
        },
        needs_recoding: {
            type: DataTypes.BOOLEAN,
        },

    })

    Address.associate = models => {
        Address.belongsTo(models.store, { foreignKey: 'store_id' })
    }

    return Address
}
