'use strict';

module.exports = {
    up: (queryInterface, Sequelize) => {

        return queryInterface.createTable('stores', {
            id: {
                allowNull: false,
                autoIncrement: true,
                primaryKey: true,
                type: Sequelize.INTEGER
            },
            license: {
                type: Sequelize.STRING
            },
            entity_name: {
                type: Sequelize.STRING
            },
            operation_type: {
                type: Sequelize.STRING
            },
            establishment_type: {
                type: Sequelize.STRING
            },
            dba_name: {
                type: Sequelize.STRING
            },
            createdAt: {
                allowNull: false,
                type: Sequelize.DATE
            },
            updatedAt: {
                allowNull: false,
                type: Sequelize.DATE
            }
        });

    },

    down: (queryInterface) => {
        return queryInterface.dropTable('stores');
    }
};
