'use strict';

module.exports = {
    up: (queryInterface, Sequelize) => {
        return queryInterface.createTable('addresses', {
            id: {
                allowNull: false,
                autoIncrement: true,
                primaryKey: true,
                type: Sequelize.INTEGER
            },
            street_name: {
                type: Sequelize.STRING
            },
            street_number: {
                type: Sequelize.STRING
            },
            address_line_2: {
                type: Sequelize.STRING,
            },
            address_line_3: {
                type: Sequelize.STRING
            },
            zip_code: {
                type: Sequelize.STRING
            },
            city: {
                type: Sequelize.STRING
            },
            state: {
                type: Sequelize.STRING
            },
            latitude: {
                type: Sequelize.DOUBLE,
            },
            longitude: {
                type: Sequelize.DOUBLE,
            },
            square_footage: {
                type: Sequelize.DOUBLE,
            },
            needs_recoding: {
                type: Sequelize.BOOLEAN,
            },
            store_id: {
                type: Sequelize.INTEGER,
                references: {
                    model: 'stores',
                    key: 'id'
                },
                onUpdate: 'CASCADE',
                onDelete: 'SET NULL'
            },

            createdAt: {
                allowNull: false,
                type: Sequelize.DATE
            },
            updatedAt: {
                allowNull: false,
                type: Sequelize.DATE
            }

        });
    },

    down: (queryInterface) => {
        return queryInterface.dropTable('addresses');
    }
};
