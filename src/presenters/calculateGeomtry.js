const Math = require('math')


exports.validateIsEmpty = ({ lat, long, comparLat, comparLong }) => !!((lat === long) && (comparLat === comparLong))

exports.geometry = ({ lat, long, comparLat, comparLong }) => {
    try {
        let radlat1 = Math.PI * lat / 180;
        let radlat2 = Math.PI * comparLat / 180;
        let theta = long - comparLong;
        let radtheta = Math.PI * theta / 180;
        let dist = Math.sin(radlat1) * Math.sin(radlat2) + Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radtheta);
        if (dist > 1) dist = 1
        dist = Math.acos(dist);
        dist = dist * 180 / Math.PI;
        dist = dist * 60 * 1.1515;
        dist = dist * 1.60934
        return dist.toFixed(2);
    } catch (err) {
        throw new Error(err)
    }

}


exports.distanceCalculate = ({ lat, long }, list) => (validateIsEmpty, geometry) => {
    return list.reduce((acc, value) => {
        let { latitude: comparLat, longitude: comparLong } = value.addresses[0]
        const validate = validateIsEmpty({ lat, long, comparLat, comparLong })
        value.dataValues.distance = validate ? 0 : geometry({ lat, long, comparLat, comparLong })
        return acc.concat(value)
    }, [])
}
