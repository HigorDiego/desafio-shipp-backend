const { isErroRequest, orderByDistance, rulesFilterDistance } = require('./case')
const { logger } = require('../../presenters/logs')
const { validateIsEmpty, geometry, distanceCalculate } = require('../../presenters/calculateGeomtry')

exports.method = 'GET'
exports.path = '/stores'

exports.middlewares = [isErroRequest]


exports.handler = async (req, res) => {
    const { lat, long } = req.query
    try {
        const { store } = require('../../database/models')
        const result = await store.findAll({ where: {}, include: { all: true } })
        const calculate = distanceCalculate({ lat, long }, result)(validateIsEmpty, geometry)
        const lists = rulesFilterDistance(orderByDistance(calculate))
        logger.info(`Response ${req.url} params lat: ${lat} e long: ${long} on time ${new Date()} status code: 200 amount stores: ${lists.length} `)
        res.status(200).json(lists)

    } catch (err) {
        logger.error(`Response ${req.url} params lat: ${lat} e long: ${long} on time ${new Date()} status code: 500 return: ${JSON.stringify(err.stack)} `)
        res.status(500).json(err)
    }
}