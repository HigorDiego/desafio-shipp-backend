
const { logger } = require('../../presenters/logs')

const isEmpty = (string) => (/(null|undefined|^$|^\d+$)/).test(string)

const isDataLocation = (location) => typeof location === 'number' && !isNaN(location) && location <= 90 && location >= -90

const errosReturn = (emptyLat, emptyLong, validateLocationLat, validateLocationLong) => {
    const array = []
    if (!emptyLat && !validateLocationLat) array.push({ title: 'Error', message: 'Latitude required!' })
    if (!emptyLong && !validateLocationLong) array.push({ title: 'Error', message: 'Long required!' })
    return array
}

exports.isErroRequest = (req, res, next) => {

    let validateLat = !isEmpty(req.query.lat)
    let validateLong = !isEmpty(req.query.long)

    const validateLocationLat = isDataLocation(parseFloat(req.query.lat))
    const validateLocationLong = isDataLocation(parseFloat(req.query.long))

    if (validateLat && validateLong && validateLocationLat && validateLocationLong) {
        logger.info(`Validate Request ${req.url} with parameters sent to api lat: ${req.query.lat} and long ${req.query.long}, on time ${new Date()} status: passed`)
        return next()
    }
    const errors = errosReturn(validateLat, validateLong, validateLocationLat, validateLocationLong)
    logger.warn(`Validate Request ${req.url} error in params send: ${JSON.stringify(errors)} on time ${new Date()} status code: 400`)
    return res.status(400).json(errors)

}

exports.orderByDistance = (arrayList) => arrayList.sort((y, x) => y.distance - x.distance)

exports.rulesFilterDistance = (arrayList) => arrayList.filter(a => a.dataValues.distance <= 6.5 && a.dataValues.lat !== null)