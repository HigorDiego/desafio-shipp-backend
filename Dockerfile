FROM node:10.13-alpine
ENV NODE_ENV production
ENV PORT 3000
ENV DB database_production.sqlite
WORKDIR /usr/src/app
COPY ["package.json", "package-lock.json*", "npm-shrinkwrap.json*", "./"]
RUN npm i sequelize-cli -g
RUN npm install --production --silent && mv node_modules ../
COPY . .
EXPOSE 3000
RUN npx sequelize db:migrate
RUN npm run import
CMD npm start